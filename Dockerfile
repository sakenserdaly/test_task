#
FROM python:3.9

#
WORKDIR /code

#
COPY ./req.txt /code/req.txt

#
RUN pip install --no-cache-dir --upgrade -r /code/req.txt

#
COPY ./app /code/app

#
CMD ["uvicorn", "app.test:app", "--host", "0.0.0.0", "--port", "80"]
