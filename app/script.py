import json
import requests
from bs4 import BeautifulSoup

def data():
    result_list=[]
    headers = {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36"
        }

    url = 'https://shop.kz/smartfony/filter/almaty-is-v_nalichii-or-ojidaem-or-dostavim/apply/'
    response = requests.get(url=url, headers=headers)
    soup = BeautifulSoup(response.text, 'lxml')
    count_of_pages = int(soup.find("div", class_="bx-pagination-container row").find_all("a")[-2].text)
    last_page = 'http://shop.kz'+soup.find("div", class_="bx-pagination-container row").find_all("a")[-1].get('href')[:-1]
    for page in range(1, count_of_pages+1)[:2]:
        response = requests.get(url=(last_page+str(page)), headers=headers)
        soup = BeautifulSoup(response.text, 'lxml')
        all_info = soup.find_all('div', class_='bx_catalog_item_container gtm-impression-product')
        for info in all_info:
            try:
                name = info.find('div', class_='bx_catalog_item_title').text.replace("Смартфон ","")
            except:
                name = 'None'
            try:
                articul = info.find('div', class_='bx_catalog_item_XML_articul').text.strip().replace("Артикул: ","")
            except:
                articul = 'None'
            try:
                price = info.find_all('span', class_='bx-more-price-text')[-1].text.replace(" ","").replace("₸","")
            except:
                price = 'None'
            try:
                memory = info.find('span', {'data-prop-title':'386'}).findNext("span").text
            except:
                memory = 'None'
            result_list.append({
                'name':name,
                'articul':articul,
                'price':price,
                'memory-size':memory
            })
    jsonString = json.dumps(result_list)
    jsonFile = open("data.json", "w")
    jsonFile.write(jsonString)
    jsonFile.close()


if __name__ == '__main__':
    data()