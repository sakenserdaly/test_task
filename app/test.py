import json
from typing import Union
from fastapi import FastAPI

app = FastAPI()

@app.get("/smartphones")
def read_root(price):
    with open('/code/app/data.json') as json_file:
        data = json.load(json_file)
        list=[]
        for info in data:
            if price == info["price"]:
                list.append(
                    info
                )
        return list
